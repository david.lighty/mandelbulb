package me.ironhorse.mandelbulb.domain;

public enum MandelbulbType {
    CUBIC,
    SPHERICAL
}
