package me.ironhorse.mandelbulb.domain;

public class Point {
    private final double x;
    private final double y;
    private final double z;
    private int stroke;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getStroke() {
        return stroke;
    }

    public Point setStroke(int stroke) {
        this.stroke = stroke;
        return this;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float toFloat(double d){
        return (float)d;
    }
}

