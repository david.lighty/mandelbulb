package me.ironhorse.mandelbulb.application;

import me.ironhorse.mandelbulb.domain.Point;
import me.ironhorse.mandelbulb.math.SphericalMandelbulb;
import peasy.PeasyCam;
import processing.core.PApplet;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Processing is a PApplet, we extend from this
 * for our "main".
 */
public class Application extends PApplet {

    public PeasyCam cam;
    private SphericalMandelbulb sphericalMandelbulb;
    private List<Point> boundedPoints;
    private int[] bounds;

    @Override
    public void settings() {
        size(1280, 1024, P3D);
    }

    @Override
    public void setup() {
        // set up camera
        cam = new PeasyCam(this, 400);
        sphericalMandelbulb = new SphericalMandelbulb(4, 10);
        boundedPoints = new ArrayList<>();

        Instant dimStart = Instant.now();
        final int dim = 256; //cube
        bounds = new int[]{-1, 1};
        // create array of points.
        for (int i = 0; i < dim; i++) {
            double x = map(i, 0, dim, bounds[0], bounds[1]);
            for (int j = 0; j < dim; j++) {
                double y = map(j, 0, dim, bounds[0], bounds[1]);
                boolean isEdge = false;
                for (int k = 0; k < dim; k++) {
                    // 0-64 dimension, re-mapped to "display"
                    double z = map(k, 0, dim, bounds[0], bounds[1]);
                    Point boundedPoint = sphericalMandelbulb.validate(new Point(x, y, z));
                    if (boundedPoint == null) {
                        isEdge = false;
                        continue;
                    }

                    if (!isEdge) {
                        isEdge = true;
                        boundedPoints.add(boundedPoint);
                    }
                }
            }
        }
        Instant dimEnd = Instant.now();
        System.out.println("All points found: " + boundedPoints.size() + " time: " + Duration.between(dimStart, dimEnd).getSeconds() + "s");
    }

    @Override
    public void draw() {

        // Draw background
        background(0);
        boundedPoints.forEach(point -> {
            int xColor = (int) Math.floor(map(point.toFloat(point.getX()), bounds[0], bounds[1], 150, 255));
            int yColor = (int) Math.floor(map(point.toFloat(point.getY()), bounds[0], bounds[1], 150, 255));
            int zColor = (int) Math.floor(map(point.toFloat(point.getZ()), bounds[0], bounds[1], 150, 255));
            stroke(xColor, yColor, zColor);
            point(point.toFloat(point.getX()) * 100, point.toFloat(point.getY()) * 100, point.toFloat(point.getZ()) * 100);
        });
    }

//    @Override
//    public void keyPressed() {
//        System.out.println("Saving frame");
//        saveFrame("bauble_####.png");
//    }

    public static void main(String[] args) {
        // Start the application
        PApplet.main(Application.class.getName());
    }

}
