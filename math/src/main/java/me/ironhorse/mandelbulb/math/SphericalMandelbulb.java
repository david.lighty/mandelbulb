package me.ironhorse.mandelbulb.math;

import me.ironhorse.mandelbulb.domain.MandelbulbType;
import me.ironhorse.mandelbulb.domain.Point;

/***
 * z = z^n + c
 * where c -> {x,y,x}
 * where z -> if 0 == c on iteration start
 * where n -> is the order of the check, default is 8
 *
 * https://www.skytopia.com/project/fractal/2mandelbulb.html
 */
public class SphericalMandelbulb {
    private final int order;
    private final int iterations;

    private MandelbulbType type = MandelbulbType.SPHERICAL;

    public SphericalMandelbulb(int order, int iterations) {
        this.order = order;
        this.iterations = iterations;
    }

    public Point validate(Point dimPoint) {
        // test our point, starting at 0 zeta
        Point zeta = new Point(0,0,0);
        if (isValidMandelbrotPoint(dimPoint, zeta, this.order, this.iterations)) {
            return dimPoint;
        }
        return null;
    }

    /**
     * Loop max iterations
     * an "r" (radius) too large fails
     */
    private boolean isValidMandelbrotPoint(Point p, Point zeta, int order, int iterations) {
        if (iterations == 0) return true;
        Point newZeta = getSphericalPoint(p, zeta, order);
        if (newZeta == null) return false;
        return isValidMandelbrotPoint(p, newZeta, order, --iterations);
    }

    /**
     * Take a point and find it's spherical location
     * "phi" and "theta" are the two angles
     * and "r" is the length from origin
     */
    private Point getSphericalPoint(Point point, Point zeta, int order) {
        double x2 = raise(zeta.getX(), 2);
        double y2 = raise(zeta.getY(), 2);
        double z2 = raise(zeta.getZ(), 2);

        double r = Math.sqrt(x2 + y2 + z2);
        if (r > 2) return null;

        double theta = Math.atan2(Math.sqrt(x2 + y2), zeta.getZ());
        double phi = Math.atan2(zeta.getY(), zeta.getX());

        double rToOrder = raise(r, order);
        double newX = rToOrder * Math.sin(theta * order) * Math.cos(phi * order);
        double newY = rToOrder * Math.sin(theta * order) * Math.sin(phi * order);
        double newZ = rToOrder * Math.cos(theta * order);

        // This is the "z"
        return new Point(newX + point.getX(), newY + point.getY(), newZ + point.getZ());
    }

    private double raise(double i, int power) {
        return Math.pow(i, power);
    }
}
