# Mandelbulb Point Cloud

Processing 3d point cloud exploration of a Mandelbulb -> https://en.wikipedia.org/wiki/Mandelbulb

![Mandelbulb Point Cloud - n = 4](./mandelbulb_256_color_n4.JPG "Mandelbulb Point Cloud - n = 4")


Tools
-----
1. Processing - for handling the draw cycle - https://processing.org/